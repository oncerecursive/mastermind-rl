# Mastermind Reinforcement learning
This project aims to create a RL-agent that learns to play the game Mastermind, and maybe in the future generalize to make into an OpenAI Gym Env.

## Rules of Mastermind
https://en.wikipedia.org/wiki/Mastermind_(board_game)

## Algorithm
Uses DQN with double both policy and target networks, warm-up, gradient clipping. Neural net itself is shallow and fully-connected.
