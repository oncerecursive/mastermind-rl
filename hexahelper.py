import sys
num = sys.argv[1]

def number_to_hexa(x):
    remainder = x
    z = []
    for k in range(3,-1,-1):
        v = remainder//6**k
        z.append(str(v))
        remainder = remainder%6**k
    print(''.join(z))

number_to_hexa(int(num))
